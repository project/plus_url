Plus Url is a simple module that gives the site owner the possibility to
configure a link to a Google+ account. The site will have a "vanity url" (ish)
at http://example.com/+ The module also provides a menu item that can be put in
the site's navigation.

FAQ:
* Why is this a module when you could just go make a menu item? That's because
drupal's .htacces does not let a url with just a + through. So you cannot make
a menu item to http://example.com/+
* Couldn't I just edit the .htacces to redirect to the profile? Yes! If you
know how to do that and have access to the file. Here is an explanation on how
to do that: https://plus.google.com/111129703735119825297/posts/KgFndrpVrum